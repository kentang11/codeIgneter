
        <!-- Left Panel -->

    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="#"><img src="" alt=""></a>
                <a class="navbar-brand hidden" href="#"><img src="" alt=""></a>
            </div>

            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="<?php echo base_url(); ?>Home"> <i class="menu-icon fa fa-home"></i>Home </a>
                    </li>
                    <h3 class="menu-title">Data</h3><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="menu-icon fa fa-table"></i>Daftar Table</a>
                      <ul class="sub-menu children dropdown-menu">

                    <li>
                        <a href="<?php echo base_url();?>Pelanggan" ><i class="fa fa-list fa-spin"></i>Pelanggan </a>
                        <a href="<?php echo base_url();?>Staff" ><i class="fa fa-users fa-spin"></i>Staff </a>
                        <a href="<?php echo base_url();?>Penyewaan" ><i class="fa fa-handshake-o fa-spin"></i>Penyewaan </a>
                        <a href="<?php echo base_url();?>Pembayaran" ><i class="fa fa-usd fa-spin"></i>Pembayaran </a>
                        <a href="<?php echo base_url();?>Mpv" ><i class="fa fa-car fa-spin"></i>MPV Mobil</a>
                        <a href="<?php echo base_url();?>Pick" ><i class="fa fa-taxi fa-spin"></i>Pick Up</a>
                    </li>
                  </li>
                </ul>





                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside><!-- /#left-panel -->
