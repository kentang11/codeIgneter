

<!doctype html>
<?php

    $this->load->View('Head');

?>
<body>

    <!-- Left Panel -->
    <?php

        $this->load->View('Menu');

    ?>

    <!-- Right Panel -->

    <div id="right-panel" class="right-panel">


              <!-- Header-->
              <header id="header" class="header">

                  <div class="header-menu">

                      <div class="col-sm-7">
                          <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
                          <div class="header-left">

                              <div class="dropdown for-message">

                                <div class="dropdown-menu" aria-labelledby="message">
                                  <p class="red">You have 4 Mails</p>
                                  <a class="dropdown-item media bg-flat-color-1" href="#">
                                      <span class="photo media-left"><img alt="avatar" src="<?php echo base_url() ;?>images/avatar/1.jpg"></span>
                                      <span class="message media-body">
                                          <span class="name float-left">Jonathan Smith</span>
                                          <span class="time float-right">Just now</span>
                                              <p>Hello, this is an example msg</p>
                                      </span>
                                  </a>
                                  <a class="dropdown-item media bg-flat-color-4" href="#">
                                      <span class="photo media-left"><img alt="avatar" src="<?php echo base_url() ;?>images/avatar/2.jpg"></span>
                                      <span class="message media-body">
                                          <span class="name float-left">Jack Sanders</span>
                                          <span class="time float-right">5 minutes ago</span>
                                              <p>Lorem ipsum dolor sit amet, consectetur</p>
                                      </span>
                                  </a>
                                  <a class="dropdown-item media bg-flat-color-5" href="#">
                                      <span class="photo media-left"><img alt="avatar" src="<?php echo base_url() ;?>images/avatar/3.jpg"></span>
                                      <span class="message media-body">
                                          <span class="name float-left">Cheryl Wheeler</span>
                                          <span class="time float-right">10 minutes ago</span>
                                              <p>Hello, this is an example msg</p>
                                      </span>
                                  </a>
                                  <a class="dropdown-item media bg-flat-color-3" href="#">
                                      <span class="photo media-left"><img alt="avatar" src="<?php echo base_url() ;?>images/avatar/4.jpg"></span>
                                      <span class="message media-body">
                                          <span class="name float-left">Rachel Santos</span>
                                          <span class="time float-right">15 minutes ago</span>
                                              <p>Lorem ipsum dolor sit amet, consectetur</p>
                                      </span>
                                  </a>
                                </div>
                              </div>
                          </div>
                      </div>

                      <div class="col-sm-5">
                          <div class="user-area dropdown float-right">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  <img class="user-avatar rounded-circle" src="<?php echo base_url() ;?>images/avt.png" alt="User Avatar">
                              </a>

                              <div class="user-menu dropdown-menu">
                                      <a class="nav-link" href="#"><i class="fa fa-user"></i>My Profile</a>

                                      <a class="nav-link" href="#"><i class="fa fa-bell"></i>Notifications <span class="count">13</span></a>

                                      <a class="nav-link" href="#"><i class="fa fa-cog"></i>Settings</a>

                                      <a class="nav-link" href="<?php echo base_url(); ?>"><i class="fa fa-key"></i>Logout</a>
                              </div>
                          </div>

                          <div class="language-select dropdown" id="language-select">
                              <a class="dropdown-toggle" href="#" data-toggle="dropdown"  id="language" aria-haspopup="true" aria-expanded="true">
                                  <i class="flag-icon flag-icon-id"></i>
                              </a>
                              <div class="dropdown-menu" aria-labelledby="language" >
                                  <div class="dropdown-item">
                                      <span class="flag-icon flag-icon-fr"></span>
                                  </div>
                                  <div class="dropdown-item">
                                      <i class="flag-icon flag-icon-es"></i>
                                  </div>
                                  <div class="dropdown-item">
                                      <i class="flag-icon flag-icon-us"></i>
                                  </div>
                                  <div class="dropdown-item">
                                      <i class="flag-icon flag-icon-it"></i>
                                  </div>
                              </div>
                          </div>

                      </div>
                  </div>

              </header><!-- /header -->
              <!-- Header-->
       <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Data Table</strong>
                        </div>
                        <div class="card-body">
                  <table id="bootstrap-data-table" class="table table-striped table-bordered">
                    <a href="<?php echo base_url(); ?>Add_mpv/ "class="btn btn-success mb-3" style="border-radius:5px;">
                      Tambah <i class="fa fa-user-plus" aria-hidden="true"></i></a>
                    <thead>

                      <tr>
                        <th>No</th>
                        <th>ID Mpv</th>
                        <th>Nama Mobil</th>
                        <th>Merek Mobil</th>
                        <th>Harga</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php
                            $no=1;
                            //print_r($b->result());die;
                            foreach ($b->result() as $hasil ) {
                        ?>
                      <tr>
                      <td><?php echo $no++ ;?></td>
                      <td><?php echo $hasil->id_mpv ;?></td>
                        <td><?php echo $hasil->nama_mobil ;?></td>
                        <td><?php echo $hasil->merek_mobil ;?></td>
                        <td><?php echo $hasil->harga ;?></td>
                        <td><a class="btn btn-primary" href="<?php echo base_url();?>Mpv/edit/<?php echo $hasil->id_mpv; ?>" style="border-radius:5px;">
                             <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                             <a class="btn btn-primary" href="<?php echo base_url();?>Mpv/hapus/<?php echo $hasil->id_mpv; ?>" style="border-radius:5px;">
                             <i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                      </tr>
                     <?php } ?>
                    </tbody>
                  </table>
                        </div>
                    </div>
                </div>


                </div>
            </div><!-- .animated -->


    </div><!-- /#right-panel -->

         <!-- .content -->
    </div><!-- /#right-panel -->

    <!-- Right Panel -->
    <?php

        $this->load->View('Down');
    ?>

</body>
</html>
