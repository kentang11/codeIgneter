<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Staff extends CI_Controller {

	public function index()
	{
		$this->load->model('Model_staff');
		$a = $this->Model_staff->get_staff();

		$input['b'] = $a;

		//print_r($a->result());die;

		$this->load->view('Tampil_staff',$input);
	}

	public function edit($id)
	{

		$a = $this->uri->segment(3);
		//print_r($a);die;
		$this->load->model('Model_staff');
		$data['edit'] = $this->Model_staff->Edit_staff($a);

		$this->load->view('Edit_staff',$data);

	}

	public function save(){

		$a=$this->input->post('nama_staff');//budi
		$g=$this->input->post('alamat');
		$j=$this->input->post('jenis_kelamin');
		$e=$this->input->post('email');
		$t=$this->input->post('tanggal_lahir');
		$id=$this->input->post('id_staff');
		//print_r($g);die;

		$data = [

			'nama_staff'=>$a,
			'alamat'=>$g,
			'jenis_kelamin'=>$j,
			'email'=>$e,
			'tanggal_lahir'=>$t
		];

		$where = [

				'id_staff'=>$id

			];

		//print_r($data);die;

		$this->load->model('Model_staff');
		$a = $this->Model_staff->update($data,$where);

		///print_r($s);die;
		redirect('Staff');

	}
	public function hapus($id){
	$this->load->model('Model_staff');
		$where = ['id_staff' => $id];
		$this->Model_staff->hapus_data($where,'staff');
		redirect('Staff');
	}
}
