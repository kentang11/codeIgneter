<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembayaran extends CI_Controller {

	public function index()
	{
		$this->load->model('Model_pembayaran');
		$a = $this->Model_pembayaran->get_pembayaran();

		$input['b'] = $a;

		//print_r($a->result());die;

		$this->load->view('Tampil_pembayaran',$input);
	}

	public function edit($id)
	{

		$a = $this->uri->segment(3);
		//print_r($a);die;
		$this->load->model('Model_pembayaran');
		$data['edit'] = $this->Model_pembayaran->Edit_pembayaran($a);

		$this->load->view('Edit_pembayaran',$data);

	}

	public function save(){

		//budi
		$g=$this->input->post('id_penyewaan');
		$j=$this->input->post('jumlah_harga');
		$e=$this->input->post('tgl_pembayaran');
		$id=$this->input->post('id_pembayaran');
		//print_r($g);die;

		$data = [


			'id_penyewaan'=>$g,
			'jumlah_harga'=>$j,
			'tgl_pembayaran'=>$e
		];

		$where = [

				'id_pembayaran'=>$id

      ];

		//print_r($data);die;

		$this->load->model('Model_pembayaran');
		$a = $this->Model_pembayaran->update($data,$where);

		///print_r($s);die;
		redirect('Pembayaran');

	}
	public function hapus($id){
	$this->load->model('Model_pembayaran');
		$where = ['id_pembayaran' => $id];
		$this->Model_pembayaran->hapus_data($where,'pembayaran');
		redirect('Pembayaran');
	}
}
