<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pick extends CI_Controller {

	public function index()
	{
		$this->load->model('Model_pick');
		$a = $this->Model_pick->get_pick();

		$input['b'] = $a;

		//print_r($a->result());die;

		$this->load->view('Tampil_pick',$input);
	}

	public function edit($id)
	{

		$a = $this->uri->segment(3);
		//print_r($a);die;
		$this->load->model('Model_pick');
		$data['edit'] = $this->Model_pick->Edit_pick($a);

		$this->load->view('Edit_pick',$data);

	}

	public function save(){

		//budi
		$g=$this->input->post('nama_mobil');
		$j=$this->input->post('merek_mobil');
		$e=$this->input->post('harga');
		$id=$this->input->post('id_pick');
		//print_r($g);die;

		$data = [


			'nama_mobil'=>$g,
			'merek_mobil'=>$j,
			'harga'=>$e
		];

		$where = [

				'id_pick'=>$id

			];

		//print_r($data);die;

		$this->load->model('Model_pick');
		$a = $this->Model_pick->update($data,$where);

		///print_r($s);die;
		redirect('Pick');

	}
	public function hapus($id){
	$this->load->model('Model_pick');
		$where = ['id_pick' => $id];
		$this->Model_pick->hapus_data($where,'pick_up');
		redirect('Pick');
	}
}
