<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelanggan extends CI_Controller {

	public function index()
	{
		$this->load->model('Model_pelanggan');
		$a = $this->Model_pelanggan->get_pelanggan();

		$input['b'] = $a;

		//print_r($a->result());die;

		$this->load->view('Tampil_pelanggan',$input);
	}

	public function edit($id)
	{

		$a = $this->uri->segment(3);
		//print_r($a);die;
		$this->load->model('Model_pelanggan');
		$data['edit'] = $this->Model_pelanggan->Edit_pelanggan($a);

		$this->load->view('Edit_pelanggan',$data);

	}

	public function save(){

		$a=$this->input->post('nama_pelanggan');//budi
		$g=$this->input->post('alamat');
		$j=$this->input->post('jenis_kelamin');
		$e=$this->input->post('email');
		$t=$this->input->post('tanggal_lahir');
		$id = $this->input->post('id_pelanggan');
		//print_r($g);die;

		$data =array(

			'nama_pelanggan'=>$a,
			'alamat'=>$g,
			'jenis_kelamin'=>$j,
			'email'=>$e,
			'tanggal_lahir'=>$t
		);

		$where = array (

				'id_pelanggan'=>$id

			);

		//print_r($data);die;

		$this->load->model('Model_pelanggan');
		$x = $this->Model_pelanggan->update($data,$where);

		///print_r($s);die;
		redirect('Pelanggan');

	}
	public function hapus($id){
	$this->load->model('Model_pelanggan');
		$where = ['id_pelanggan' => $id];
		$this->Model_pelanggan->hapus_data($where,'pelanggan');
		redirect('Pelanggan');
	}
}
