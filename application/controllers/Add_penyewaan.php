<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Add_penyewaan extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Model_penyewaan');
	}

	public function index()
	{
		//print_r(base_url());die;
		$data['pelanggan'] = $this->Model_penyewaan->getPelanggan();
		$this->load->view('View_add_datapenyewaan', $data);
	}

	public function add(){

		// $a=$_POST[''];//post['name yang input-html']
		$this->load->view('View_add_datapenyewaan');
		$t=$this->input->post('jumlah');
    $p=$this->input->post('tgl_penyewaan');
    $b=$this->input->post('tgl_pengembalian');
		//print_r($g);die;

		$data = [

			'jumlah'=>$t,
      'tgl_penyewaan'=>$p,
      'tgl_pengembalian'=>$b
		];
		$this->Model_penyewaan->add_datapenyewaan($data);
		//print_r($data);die;

		// array(

		// 	'field1 dari table database yang kita punya',
		// 	'field2',
		// 	'field3'

		// 	);
		redirect('Penyewaan');
	}
}
