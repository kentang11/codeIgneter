<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penyewaan extends CI_Controller {

	public function index()
	{
		$this->load->model('Model_penyewaan');
		$a = $this->Model_penyewaan->get_penyewaan();

		$input['b'] = $a;

		//print_r($a->result());die;

		$this->load->view('Tampil_penyewaan',$input);
	}

	public function edit($id)
	{

		$a = $this->uri->segment(3);
		//print_r($a);die;
		$this->load->model('Model_penyewaan');
		$data['edit'] = $this->Model_penyewaan->Edit_penyewaan($a);

		$this->load->view('Edit_penyewaan',$data);

	}

	public function save(){

		//budi
		$g=$this->input->post('id_pelanggan');
		$j=$this->input->post('id_mobil');
		$e=$this->input->post('id_staff');
		$t=$this->input->post('jumlah');
    $p=$this->input->post('tgl_penyewaan');
    $b=$this->input->post('tgl_pengembalian');
		$id=$this->input->post('id_penyewaan');
		//print_r($g);die;

		$data = [


			'id_pelanggan'=>$g,
			'id_mobil'=>$j,
			'id_staff'=>$e,
			'jumlah'=>$t,
      'tgl_penyewaan'=>$p,
      'tgl_pengembalian'=>$b
		];

		$where = [

				'id_penyewaan'=>$id

      ];

		//print_r($data);die;

		$this->load->model('Model_penyewaan');
		$a = $this->Model_penyewaan->update($data,$where);

		///print_r($s);die;
		redirect('Penyewaan');

	}
	public function hapus($id){
	$this->load->model('Model_penyewaan');
		$where = ['id_penyewaan' => $id];
		$this->Model_penyewaan->hapus_data($where,'penyewaan');
		redirect('Penyewaan');
	}
}
