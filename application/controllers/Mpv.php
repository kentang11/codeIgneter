<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mpv extends CI_Controller {

	public function index()
	{
		$this->load->model('Model_mpv');
		$a = $this->Model_mpv->get_mpv();

		$input['b'] = $a;

		//print_r($a->result());die;

		$this->load->view('Tampil_mpv',$input);
	}

	public function edit($id)
	{

		$a = $this->uri->segment(3);
		//print_r($a);die;
		$this->load->model('Model_mpv');
		$data['edit'] = $this->Model_mpv->Edit_mpv($a);

		$this->load->view('Edit_mpv',$data);

	}

	public function save(){

		//budi
		$g=$this->input->post('nama_mobil');
		$j=$this->input->post('merek_mobil');
		$e=$this->input->post('harga');
		$id=$this->input->post('id_mpv');
		//print_r($g);die;

		$data = [


			'nama_mobil'=>$g,
			'merek_mobil'=>$j,
			'harga'=>$e
		];

		$where = [

				'id_mpv'=>$id

			];

		//print_r($data);die;

		$this->load->model('Model_mpv');
		$a = $this->Model_mpv->update($data,$where);

		///print_r($s);die;
		redirect('Mpv');

	}
	public function hapus($id){
	$this->load->model('Model_mpv');
		$where = ['id_mpv' => $id];
		$this->Model_mpv->hapus_data($where,'mpv_mobil');
		redirect('Mpv');
	}
}
