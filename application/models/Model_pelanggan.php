<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_pelanggan extends CI_Model {


	public function get_pelanggan(){

		$this->db->select('*');
		return $this->db->get('pelanggan');

	}

	public function add_datapelanggan($input){

		//insert into tablepasien values("");
		$this->db->insert('pelanggan',$input);

	}

	public function Edit_pelanggan($a){

		$this->db->where('id_pelanggan',$a);
		return $this->db->get('pelanggan');
	}

	public function update($data,$where){

		 $this->db->where($where);
		 $this->db->update('pelanggan',$data);

	}

	public function hapus_data($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
	}
}
