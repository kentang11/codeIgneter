<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_penyewaan extends CI_Model {


	public function get_penyewaan(){

		$this->db->select('*');
		return $this->db->get('penyewaan');

	}

	public function getPelanggan() {
		$sql = " SELECT id_pelanggan, nama_pelanggan FROM pelanggan ";
		$data = $this->db->query($sql);
		return $data->result_array();
	}

	public function add_datapenyewaan($input){

		//insert into tablepasien values("");
		$this->db->insert('penyewaan',$input);

	}

	public function Edit_penyewaan($a){

		$this->db->where('id_penyewaan',$a
                      );
		return $this->db->get('penyewaan');
	}

	public function update($data,$where){

		 $this->db->where($where);
		 $this->db->update('penyewaan',$data);

	}

	public function hapus_data($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
	}
}
