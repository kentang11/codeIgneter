<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_staff extends CI_Model {


	public function get_staff(){

		$this->db->select('*');
		return $this->db->get('staff');

	}

	public function add_datastaff($input){

		//insert into tablepasien values("");
		$this->db->insert('staff',$input);

	}

	public function Edit_staff($a){

		$this->db->where('id_staff',$a);
		return $this->db->get('staff');
	}

	public function update($data,$where){

		 $this->db->where($where);
		 $this->db->update('staff',$data);

	}

	public function hapus_data($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
	}
}
