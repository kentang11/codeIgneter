<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_pick extends CI_Model {


	public function get_pick(){

		$this->db->select('*');
		return $this->db->get('pick_up');

	}

	public function add_datapick($input){

		//insert into tablepasien values("");
		$this->db->insert('pick_up',$input);

	}

	public function Edit_pick($a){

		$this->db->where('id_pick',$a);
		return $this->db->get('pick_up');
	}

	public function update($data,$where){

		 $this->db->where($where);
		 $this->db->update('pick_up',$data);

	}

	public function hapus_data($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
	}
}
