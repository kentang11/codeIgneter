<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_mpv extends CI_Model {


	public function get_mpv(){

		$this->db->select('*');
		return $this->db->get('mpv_mobil');

	}

	public function add_datampv($input){

		//insert into tablepasien values("");
		$this->db->insert('mpv_mobil',$input);

	}

	public function Edit_mpv($a){

		$this->db->where('id_mpv',$a);
		return $this->db->get('mpv_mobil');
	}

	public function update($data,$where){

		 $this->db->where($where);
		 $this->db->update('mpv_mobil',$data);

	}

	public function hapus_data($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
	}
}
