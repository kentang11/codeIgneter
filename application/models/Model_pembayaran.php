<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_pembayaran extends CI_Model {


	public function get_pembayaran(){

		$this->db->select('*');
		return $this->db->get('pembayaran');

	}

	public function add_datapembayaran($input){

		//insert into tablepasien values("");
		$this->db->insert('pembayaran',$input);

	}

	public function Edit_pembayaran($a){

		$this->db->where('id_pembayaran',$a);
		return $this->db->get('pembayaran');
	}

	public function update($data,$where){

		 $this->db->where($where);
		 $this->db->update('pembayaran',$data);

	}

	public function hapus_data($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
	}
}
